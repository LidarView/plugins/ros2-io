// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: Apache-2.0

#ifndef vtkMCAPReader_h
#define vtkMCAPReader_h

#include <vtkEmulatedTimeAlgorithm.h>

#include <map>
#include <memory>

#include "MCAPReaderModule.h" // for export macro

class MCAPREADER_EXPORT vtkMCAPReader : public vtkEmulatedTimeAlgorithm
{
public:
  static vtkMCAPReader* New();
  vtkTypeMacro(vtkMCAPReader, vtkEmulatedTimeAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Name of the file that will be opened
   */
  vtkSetFilePathMacro(FileName);
  vtkGetFilePathMacro(FileName);

  int GetTopicArrayStatus(const char* name);
  void SetTopicArrayStatus(const char* name, int status);
  int GetNumberOfTopicArrays();
  const char* GetTopicArrayName(int idx);

protected:
  vtkMCAPReader();
  ~vtkMCAPReader();

  int FillOutputPortInformation(int port, vtkInformation* info);

  /**
   * Core implementation of the data set reader
   */
  int RequestInformation(vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector) override;
  int RequestData(vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector) override;

private:
  vtkMCAPReader(const vtkMCAPReader&) = delete;
  void operator=(const vtkMCAPReader&) = delete;

  char* FileName;

  std::vector<std::string> TopicsNames;
  std::map<std::string, int> AvailablesTopicsSwitch;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
