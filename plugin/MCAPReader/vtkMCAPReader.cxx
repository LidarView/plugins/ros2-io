// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: Apache-2.0

#include "vtkMCAPReader.h"

#include <vtkCellArray.h>
#include <vtkDataArray.h>
#include <vtkDataObject.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkMemoryResourceStream.h>
#include <vtkNew.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkResourceParser.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkTypeInt16Array.h>
#include <vtkTypeInt32Array.h>
#include <vtkTypeInt8Array.h>
#include <vtkTypeUInt16Array.h>
#include <vtkTypeUInt32Array.h>
#include <vtkTypeUInt8Array.h>
#include <vtkVariant.h>
#include <vtkVertexGlyphFilter.h>

#include <fastcdr/Cdr.h>

#include "vtkMCAPInclude.h"

#include <array>

namespace
{
void OnProblemCallback(const mcap::Status& status)
{
  vtkGenericWarningMacro("Mcap reader internal error \"" << status.message << "\"");
}

double ConvertToSeconds(const uint64_t& value)
{
  return static_cast<double>(value / 1e9);
}

constexpr std::array<const char*, 3> coordsFields = { "x", "y", "z" };
constexpr std::array<const char*, 1> supportedMsg = { "sensor_msgs/msg/PointCloud2" };
constexpr const char* ros2Encoding = "ros2msg";
}

//-----------------------------------------------------------------------------
class vtkMCAPReader::vtkInternals
{
private:
  typedef enum
  {
    INT8 = 1,
    UINT8 = 2,
    INT16 = 3,
    UINT16 = 4,
    INT32 = 5,
    UINT32 = 6,
    FLOAT32 = 7,
    FLOAT64 = 8
  } Datatype;

  typedef struct
  {
    std::string name;
    uint32_t offset;
    Datatype datatype;
    uint32_t count;
  } PointField;

  typedef struct
  {
    // header
    uint32_t sequence;
    uint16_t timeSecs;
    uint16_t timeNsecs;
    std::string frameId;

    // body
    uint32_t height;
    uint32_t width;
    std::vector<PointField> fields;
    bool isBigendian;
    uint32_t pointStep;
    uint32_t rowStep;
    bool isDense;
  } PointCloud2;

  //-----------------------------------------------------------------------------
  template <typename T>
  static bool ContainsCoords(const std::vector<T>& fields)
  {
    for (auto const& elem : coordsFields)
    {
      if (std::find(fields.begin(), fields.end(), elem) == fields.end())
      {
        return false;
      }
    }
    return true;
  }

  //-----------------------------------------------------------------------------
  template <typename T>
  static T ReadStream(vtkSmartPointer<vtkMemoryResourceStream>& stream)
  {
    T value;
    stream->Read(&value, sizeof(T));
    if (stream->EndOfStream())
    {
      vtkGenericWarningMacro("Stream out of buffer.");
    }
    return value;
  }

  //-----------------------------------------------------------------------------
  vtkVariant ReadStreamWithDataType(Datatype dataType)
  {
    switch (dataType)
    {
      case INT8:
        return vtkInternals::ReadStream<int8_t>(this->Stream);
      case UINT8:
        return vtkInternals::ReadStream<uint8_t>(this->Stream);
      case INT16:
        return vtkInternals::ReadStream<int16_t>(this->Stream);
      case UINT16:
        return vtkInternals::ReadStream<uint16_t>(this->Stream);
      case INT32:
        return vtkInternals::ReadStream<int32_t>(this->Stream);
      case UINT32:
        return vtkInternals::ReadStream<uint32_t>(this->Stream);
      case FLOAT32:
        return vtkInternals::ReadStream<float>(this->Stream);
      case FLOAT64:
      default:
        return vtkInternals::ReadStream<double>(this->Stream);
    }
  }

  //-----------------------------------------------------------------------------
  vtkDataArray* CreateNewDataArray(Datatype dataType)
  {
    switch (dataType)
    {
      case INT8:
        return vtkTypeInt8Array::New();
      case UINT8:
        return vtkTypeUInt8Array::New();
      case INT16:
        return vtkTypeInt16Array::New();
      case UINT16:
        return vtkTypeUInt16Array::New();
      case INT32:
        return vtkTypeInt32Array::New();
      case UINT32:
        return vtkTypeUInt32Array::New();
      case FLOAT32:
        return vtkFloatArray::New();
      case FLOAT64:
      default:
        return vtkDoubleArray::New();
    }
  }

  //-----------------------------------------------------------------------------
  bool DeserializeCDRMessage(const char* data, uint64_t dataSize, PointCloud2& info)
  {
    char* bufferPtr = const_cast<char*>(data);
    auto cdrBuffer = std::make_unique<eprosima::fastcdr::FastBuffer>(bufferPtr, dataSize);
    auto cdr = std::make_unique<eprosima::fastcdr::Cdr>(
      *cdrBuffer, eprosima::fastcdr::Cdr::DEFAULT_ENDIAN, eprosima::fastcdr::CdrVersion::DDS_CDR);

    cdr->read_encapsulation();

    // Deserialize header
    cdr->deserialize(info.sequence);
    cdr->deserialize(info.timeSecs);
    cdr->deserialize(info.timeNsecs);
    cdr->deserialize(info.frameId);

    cdr->deserialize(info.height);
    cdr->deserialize(info.width);

    // Deserialize fields information
    uint32_t fieldSize;
    cdr->deserialize(fieldSize);
    info.fields.reserve(fieldSize);
    for (unsigned idx = 0; idx < fieldSize; idx++)
    {
      PointField field;
      uint8_t datatype;
      cdr->deserialize(field.name);
      cdr->deserialize(field.offset);
      cdr->deserialize(datatype);
      cdr->deserialize(field.count);
      if (datatype == 0 || datatype > 8)
      {
        vtkGenericWarningMacro("Unknown ROS datatype.");
        return false;
      }
      field.datatype = static_cast<Datatype>(datatype);
      info.fields.emplace_back(field);
    }

    cdr->deserialize(info.isBigendian);
    cdr->deserialize(info.pointStep);
    cdr->deserialize(info.rowStep);

    uint32_t dataArraySize;
    cdr->deserialize(dataArraySize);
    if (dataArraySize != info.pointStep * info.width * info.height)
    {
      vtkGenericWarningMacro("Incoherent data array size, result may be invalid.");
    }
    // Ignore raw data and set vtkStream
    this->Stream->SetBuffer(cdr->get_current_position(), dataArraySize, false);
    cdr->jump(dataArraySize);

    cdr->deserialize(info.isDense);
    return true;
  }

  //-----------------------------------------------------------------------------
  bool CheckSupported(const PointCloud2& info)
  {
    if (info.isBigendian)
    {
      vtkGenericWarningMacro("Cannot parse big endian ROS message.");
      return false;
    }

    std::vector<std::string> fieldNames;
    for (const auto& field : info.fields)
    {
      fieldNames.emplace_back(field.name);
    }
    if (!vtkInternals::ContainsCoords(fieldNames))
    {
      vtkGenericWarningMacro("This ROS message does not contains point information.");
      return false;
    }

    for (const auto& field : info.fields)
    {
      if (field.count != 1)
      {
        vtkGenericWarningMacro("This message contains a field with more that one "
          << "scalars, the reader doesn't support parsing vectors yet.");
        return false;
      }
    }
    return true;
  }

  //-----------------------------------------------------------------------------
  void ParseBufferToVTKData(vtkPolyData* outputData, const PointCloud2& info)
  {
    uint32_t numberOfPoints = info.width * info.height;

    // Create points and fields and allocate memory
    vtkNew<vtkPoints> points;
    vtkNew<vtkCellArray> cells;
    points->SetDataTypeToDouble();
    points->SetNumberOfPoints(numberOfPoints);
    cells->AllocateEstimate(numberOfPoints, 1);

    std::vector<vtkSmartPointer<vtkDataArray>> tupleArrays;
    tupleArrays.resize(info.fields.size());

    for (unsigned fieldIdx = 0; fieldIdx < info.fields.size(); fieldIdx++)
    {
      const auto& field = info.fields[fieldIdx];
      auto it = std::find(coordsFields.begin(), coordsFields.end(), field.name);
      if (it == coordsFields.end())
      {
        auto tupleIt = tupleArrays.begin();
        std::advance(tupleIt, fieldIdx);
        tupleArrays.emplace(tupleIt, this->CreateNewDataArray(field.datatype));
        tupleArrays[fieldIdx]->SetName(field.name.c_str());
        tupleArrays[fieldIdx]->SetNumberOfComponents(1);
        tupleArrays[fieldIdx]->SetNumberOfTuples(numberOfPoints);
        outputData->GetPointData()->AddArray(tupleArrays[fieldIdx]);
      }
    }

    // Fill point information with buffer stream
    for (vtkIdType arrayIdx = 0; arrayIdx < numberOfPoints; arrayIdx++)
    {
      double point[3];
      for (unsigned fieldIdx = 0; fieldIdx < info.fields.size(); fieldIdx++)
      {
        const auto& field = info.fields[fieldIdx];

        // Add field padding if necessary
        uint32_t expectedOffset = arrayIdx * info.pointStep + field.offset;
        if (this->Stream->Tell() != expectedOffset)
        {
          this->Stream->Seek(expectedOffset, vtkResourceStream::SeekDirection::Begin);
        }
        vtkVariant value = this->ReadStreamWithDataType(field.datatype);

        auto it = std::find(coordsFields.begin(), coordsFields.end(), field.name);
        if (it != coordsFields.end())
        {
          size_t coordIdx = std::distance(coordsFields.begin(), it);
          point[coordIdx] = value.ToDouble();
        }
        else
        {
          tupleArrays[fieldIdx]->SetVariantValue(arrayIdx, value);
        }
      }
      points->SetPoint(arrayIdx, point);
      cells->InsertNextCell(1, &arrayIdx);
    }

    outputData->SetPoints(points);
    outputData->SetVerts(cells);
  }

public:
  vtkInternals() { this->Stream = vtkSmartPointer<vtkMemoryResourceStream>::New(); }

  //-----------------------------------------------------------------------------
  bool DecodeMessage(const char* data, uint64_t dataSize, vtkPolyData* outputData)
  {
    PointCloud2 pointCloudInfo;

    if (!this->DeserializeCDRMessage(data, dataSize, pointCloudInfo))
    {
      return false;
    }

    if (!this->CheckSupported(pointCloudInfo))
    {
      return false;
    }

    this->ParseBufferToVTKData(outputData, pointCloudInfo);
    return true;
  }

public:
  std::vector<double> DisplayTimesteps;
  std::vector<uint64_t> AvailableLogTimes;

private:
  vtkSmartPointer<vtkMemoryResourceStream> Stream;
};

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkMCAPReader);

//----------------------------------------------------------------------------
vtkMCAPReader::vtkMCAPReader()
  : Internals(new vtkMCAPReader::vtkInternals())
{
  this->FileName = nullptr;

  this->SetNumberOfInputPorts(0);
  this->SetNumberOfOutputPorts(1);
}

//----------------------------------------------------------------------------
vtkMCAPReader::~vtkMCAPReader()
{
  delete[] this->FileName;
}

//-----------------------------------------------------------------------------
int vtkMCAPReader::FillOutputPortInformation(int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
  return 1;
}

//-----------------------------------------------------------------------------
int vtkMCAPReader::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** vtkNotUsed(inputVector), vtkInformationVector* outputVector)
{
  // Get the info object
  vtkInformation* info = outputVector->GetInformationObject(0);

  vtkNew<vtkPolyData> pointsPolyData;

  mcap::McapReader reader;

  auto isOpenStatus = reader.open(this->FileName);
  if (!isOpenStatus.ok())
  {
    vtkErrorMacro(
      "Could not open " << this->FileName << " mcap file. (\"" << isOpenStatus.message << "\")");
    return 0;
  }

  auto summaryStatus =
    reader.readSummary(mcap::ReadSummaryMethod::AllowFallbackScan, ::OnProblemCallback);
  if (!summaryStatus.ok())
  {
    return 0;
  }

  double requestedTime = info->Get(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP());
  std::vector<double>& timeValues = this->Internals->DisplayTimesteps;
  size_t index = std::distance(
    timeValues.begin(), std::lower_bound(timeValues.begin(), timeValues.end(), requestedTime));

  size_t logTimeSize = this->Internals->AvailableLogTimes.size();
  if (index < logTimeSize)
  {
    mcap::ReadMessageOptions options;
    options.startTime = this->Internals->AvailableLogTimes.at(index);
    if (index + 1 < logTimeSize)
    {
      options.endTime = this->Internals->AvailableLogTimes.at(index + 1);
    }
    options.topicFilter = [this](std::string_view view)
    { return this->AvailablesTopicsSwitch[std::string(view)]; };

    mcap::LinearMessageView messageView = reader.readMessages(::OnProblemCallback, options);
    for (auto& view : messageView)
    {
      if (view.schema)
      {
        if (std::find(supportedMsg.begin(), supportedMsg.end(), view.schema->name) ==
            supportedMsg.end() ||
          view.schema->encoding != ros2Encoding)
        {
          continue;
        }
        const char* buffer = reinterpret_cast<const char*>(view.message.data);
        this->Internals->DecodeMessage(buffer, view.message.dataSize, pointsPolyData);
      }
    }
  }
  reader.close();

  vtkPolyData* output = vtkPolyData::SafeDownCast(info->Get(vtkDataObject::DATA_OBJECT()));
  output->ShallowCopy(pointsPolyData);

  return 1;
}

//-----------------------------------------------------------------------------
int vtkMCAPReader::RequestInformation(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** vtkNotUsed(inputVector), vtkInformationVector* outputVector)
{
  vtkInformation* outInfo = outputVector->GetInformationObject(0);

  mcap::McapReader reader;
  auto isOpenStatus = reader.open(this->FileName);
  if (!isOpenStatus.ok())
  {
    vtkWarningMacro(
      "Could not open " << this->FileName << " mcap file. (\"" << isOpenStatus.message << "\")");
    return 0;
  }

  auto summaryStatus =
    reader.readSummary(mcap::ReadSummaryMethod::AllowFallbackScan, ::OnProblemCallback);
  if (!summaryStatus.ok())
  {
    return 0;
  }

  const auto& schemas = reader.schemas();
  const auto& channels = reader.channels();
  for (const auto& [id, channelPtr] : channels)
  {
    const auto& schema = schemas.at(channelPtr->schemaId);
    if (std::find(supportedMsg.begin(), supportedMsg.end(), schema->name) != supportedMsg.end())
    {
      this->TopicsNames.emplace_back(channelPtr->topic);
      this->AvailablesTopicsSwitch.emplace(channelPtr->topic, 1);
    }
  }

  this->Internals->DisplayTimesteps.clear();
  this->Internals->AvailableLogTimes.clear();
  const auto& stats = reader.statistics();
  if (stats.has_value())
  {
    mcap::Statistics statsValue = stats.value();
    this->Internals->DisplayTimesteps.reserve(statsValue.messageCount);
    this->Internals->AvailableLogTimes.reserve(statsValue.messageCount);
  }

  mcap::ReadMessageOptions options;
  options.topicFilter = [this](std::string_view view)
  { return this->AvailablesTopicsSwitch[std::string(view)]; };
  mcap::LinearMessageView messageView = reader.readMessages(::OnProblemCallback, options);

  for (auto& view : messageView)
  {
    this->Internals->DisplayTimesteps.emplace_back(::ConvertToSeconds(view.message.publishTime));
    this->Internals->AvailableLogTimes.emplace_back(view.message.logTime);
  }

  double timeRange[2];
  timeRange[0] = this->Internals->DisplayTimesteps.front();
  timeRange[1] = this->Internals->DisplayTimesteps.back();
  outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_STEPS(),
    this->Internals->DisplayTimesteps.data(), this->Internals->DisplayTimesteps.size());
  outInfo->Set(vtkStreamingDemandDrivenPipeline::TIME_RANGE(), timeRange, 2);

  reader.close();
  return 1;
}

//-----------------------------------------------------------------------------
int vtkMCAPReader::GetTopicArrayStatus(const char* name)
{
  assert(name != nullptr);
  std::map<std::string, int>::const_iterator it = this->AvailablesTopicsSwitch.find(name);
  if (it != this->AvailablesTopicsSwitch.end())
  {
    return it->second;
  }
  vtkErrorMacro(<< "Topic '" << name << "' not found.");
  return 0;
}

//-----------------------------------------------------------------------------
void vtkMCAPReader::SetTopicArrayStatus(const char* name, int status)
{
  assert(name != nullptr);
  this->AvailablesTopicsSwitch[name] = status;

  // IMPORTANT: Let VTK know, that the pipeline needs an update
  // (otherwise nothing would happen)
  this->Modified();
}

//-----------------------------------------------------------------------------
int vtkMCAPReader::GetNumberOfTopicArrays()
{
  return static_cast<int>(this->TopicsNames.size());
}

//-----------------------------------------------------------------------------
const char* vtkMCAPReader::GetTopicArrayName(int idx)
{
  assert(idx < this->TopicsNames.size());
  return this->TopicsNames[idx].c_str();
}

//----------------------------------------------------------------------------
void vtkMCAPReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << "vtkMCAPReader" << std::endl;
  os << "Filename: " << this->FileName << std::endl;
}
