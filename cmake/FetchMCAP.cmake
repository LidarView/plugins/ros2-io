include(FetchContent)

function (add_mcap_library include_dir)
    add_library(MCAP::mcap INTERFACE IMPORTED)
    set_target_properties(MCAP::mcap
    PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${include_dir}")
endfunction ()

function (find_mcap_library)
    find_path(mcap_INCLUDE_DIR
    NAMES
        mcap/mcap.hpp
    DOC "MCAP include directory")

    add_mcap_library(${mcap_INCLUDE_DIR})
endfunction ()

function (fetch_mcap_library)
    message(STATUS "Fetching MCAP library")
    FetchContent_Declare(mcap
        URL https://github.com/foxglove/mcap/archive/refs/tags/releases/cpp/v1.3.0.zip
        URL_HASH SHA256=f0c931f5c3a949a15224cf53daf4f2be2fd99c8b9efd80d14de27615f0acc99e
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
    )

    FetchContent_GetProperties(mcap)
    if (NOT mcap_POPULATED)
        FetchContent_Populate(mcap)
    endif()

    add_mcap_library("${mcap_SOURCE_DIR}/cpp/mcap/include/")
endfunction ()
