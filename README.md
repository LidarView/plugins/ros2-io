# ROS2 IO LidarView Plugin

The primary objective of this plugin is to be able to read ROS2 bags into LidarView (and ParaView).

## Dependencies

- [ParaView 5.12](https://gitlab.kitware.com/paraview/paraview)
- [fastcdr 2.1.0](https://github.com/eProsima/Fast-CDR)
- (Optional) [zstd 1.5.0](https://github.com/facebook/zstd)
- (Optional) [lz4 1.9.3](https://github.com/lz4/lz4)

## Build and load the plugin

Follow these steps to build and load the GIS Tools plugin:

1. Clone the repository, including its submodules:
```bash
git clone https://gitlab.kitware.com:LidarView/plugins/ros2-io.git
```
2. Create a build folder and navigate into it:
```bash
mkdir build && cd build
```
3. Run CMake to configure the build. Make sure to specify the path to your ParaView CMake directory using the -DParaView_DIR option:
```bash
`cmake ../ros2-io -GNinja -DParaView_DIR=<path/to/paraview/cmake/dir> -Dfastcdr_DIR=<path/to/fastcdr/cmake/dir>`
```
4. Build the plugin
```bash
cmake --build .
```
5. Start LidarView or ParaView and follow these steps to load the plugin:
    1. Navigate to `Tools > Manage Plugins`.
    2. Click on `Load New ...`.
    3. Select the plugin file (.so or .dll) located in `<path/to/build>/lib/paraview-5.11/plugins/ROS2IO/`.

## License

This code is distributed under the Apache 2.0 license.
